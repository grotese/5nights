# README #
-----------------------
Repo for my new Spooky Simulator Game.  
**NOW WITH LEAN TECHNOLOGY!**

### Working Titles ###
* 5 Nights at the Fredrickson's
* Spagett Simulator 2: Electric Spookaloo
* Adventures in Babysitting 2: Electric Spookaloo

### TODO ###
-----------------------
* **( DONE )** Camera View Minimap based off Wall Meshes
* **( DONE )** On Camera Player Minimap, display locations of all security cameras and the areas they can view
* **( DONE )** Allow Switching to security Camera views by pressing on dynamic camera UI element
* **( DONE )** Add Cameras to Monster path nodes to allow monster to fuck their shit up, when the camera player is watching them.
* **( DONE )** Camera Static Effect
* **( DONE )** FEED OFFLINE Text over Static Screen
* **( DONE )** Fuzzing Camera Effect ( for when camera is on )
* **( DONE )** Apply Camera Static Effect during Monster movement
* **( DONE )**Ground Player Lean Ability
* **( DONE )** Highlight Active Security Camera on Minimap ( Change Security Camera Buttons to toggles )
* **( DONE )** Make Monster Track Player State to just stare at the player until they leave the monster's view, then return to patrol state
* **( DONE )** Fix LighFlicker null issue over the network ( Photon is trying to run an RPC even though the local object is null - shouldn't photon figure that out and just ignore it? )
* **( DONE )** **Apparently, I had already done that.** Move ground player UI Create and Player association logic into the actual ground player behavior, from the global network handler.  I can't get the UI as a child object, but at least I can break up that monolith known as NetworkManager.cs.
* **( DONE )** Synchronize Monster States accross the WORLD WIDE WEB, in order for some states to have local behavior like watching that fine player.
* **( DONE )** Make States Syncronize themselves instead of having one big syncorizer
* **( DONE )** Create Death Screen w/ Spagett!
* **( DONE )** Implement Ground Player Kill
* **( DONE )** Refactor Monster State machine to avoid Unity's ambiguity when it comes to messages
* Refactor Mouse Look Controls to support Corner Leaning and Potential Screen Shake
    * Fix Snap back to center due to MouseLook not ignoring rotation around Y axis
    * Fix Headbob after introduction of ground player leaning
* **( DONE )** Camera Change Transition Effect
* Create Monster Find Music Box State to track down position where music box was last played
* **( DONE )** Add Static Screen Noise
* **( DONE ) SEE BELOW** Only Allow Static Screen Noises to be heard from Camera Player ( locally ), or RPC those guys
* **( DONE )** Refactor Camera Player to automatically find the Camera Monitoring System and add itself, associate itself with the camera system. Instead of going through the network manager.
* Refactor Minimap Handler to break apart camera system into multiple components - it's getting too big.
* Figure out why the standalone game crashes on exit and also what sending message header failed means
* Create System to pipe all outside Audio Sounds into door Source, for Camera Player so he/she can't just hear shit through walls.
* **( DONE )** Add Sound to indicate monster has seen player, before murdering them
* **( DONE )** Add Power Resource ( generator ) object that the Camera System has a window into.
* **( DONE )** Add Generator Game Object that Ground Player can activate and recharge.
* **( DONE )** Shut down camera player controls if power goes out
    * **( DONE )** Restart Cameras when power returns
* **( DONE )** Fix Desyncing of Power Resource due to race conditions of client/server.  Fix by changing RPC to only send to owner, and create OnSerialize method to sync owner's power value with everyone elses'.
* On Camera Power On, if static noise was playing before power went out, return to playing static noise, due to disabled camera.
* Have Doors block off monster movement

## In Engine Screen Shots ##
-----------------------
![Alt text](http://media.giphy.com/media/nGybKUNCAX0cM/giphy.gif "AI State: Begin Spook")

![Alt text](https://allaboutjohnmcblain.files.wordpress.com/2013/10/demon-face.gif?w=560 "Spook in Progress")

![Alt text](http://giant.gfycat.com/AggressiveFamousCob.gif)

### What is this repository for? ###
-----------------------
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)