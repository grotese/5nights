﻿using UnityEngine;
using UnityEngine.UI;

// Type of player that each user can join as ( only one per game )
public enum PlayerType : byte
{
	CameraPlayer,
	GroundPlayer
}

public class NetworkManager : MonoBehaviour 
{
	[SerializeField] private Text connectionText;
	[SerializeField] private Transform cameraRoomSpawn;
	[SerializeField] private Transform groundSpawn;
	[SerializeField] private Camera waitingRoomCamera;
	[SerializeField] private GameObject roomJoinMenu;

	[SerializeField] private string groundPlayerPrefabName = "FPSPlayer";
	[SerializeField] private string cameraPlayerPrefabName = "CameraPlayer";

	// Use this for initialization
	void Start () 
	{
		PhotonNetwork.logLevel = PhotonLogLevel.ErrorsOnly;
		PhotonNetwork.ConnectUsingSettings("0.1");
	}
	
	// Update is called once per frame
	void Update () 
	{
		connectionText.text = PhotonNetwork.connectionStateDetailed.ToString() + ", Ping: " + PhotonNetwork.GetPing();
	}

	void OnJoinedLobby()
	{
		// Join room or create room, it it doesn't already exist
		RoomOptions ro = new RoomOptions
		{
			isVisible = true,
			maxPlayers = 2
		};

		PhotonNetwork.JoinOrCreateRoom( "TestRoom", ro, TypedLobby.Default );
	}

	void OnJoinedRoom()
	{
		// Spawn the Join Room Menu, ya asshole
		GameObject room_menu= Instantiate( roomJoinMenu );
		RoomJoinScript room_join_script = room_menu.GetComponent<RoomJoinScript>();
		room_join_script.localNetworkManager = this;
	}

	public void SpawnLocalPlayer( PlayerType player_type )
	{
		Transform spawn_transform = player_type == PlayerType.CameraPlayer ?
			cameraRoomSpawn :
			groundSpawn;

		string prefab_name = player_type == PlayerType.CameraPlayer ?
			cameraPlayerPrefabName :
			groundPlayerPrefabName;

		PhotonNetwork.Instantiate( 
			prefab_name,                 // Prefab name
			spawn_transform.position,    // position
			spawn_transform.rotation,    // rotation
			0                            // Group Index?
		);

		SetLobbyCamera( false );     // Turn off lobby camera
	}

	private void SetLobbyCamera( bool camera_enabled )
	{
		waitingRoomCamera.enabled = camera_enabled;
		// Attempt to find audio listener and set that, it it exists
		AudioListener camera_listener = waitingRoomCamera.GetComponent<AudioListener>();
		if ( camera_listener != null )
		{
			camera_listener.enabled = camera_enabled;
		}
	}
}
