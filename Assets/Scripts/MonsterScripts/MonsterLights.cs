﻿using UnityEngine;
using System.Collections;

public class MonsterLights : Photon.MonoBehaviour 
{
	public MonsterHandler monsterObject = null;
	public Light lightObject = null;
	private float elapsedTime = 0.0f;

	private float timeVisible             = 5.0f;
	private float timeFlickering          = 3.0f;
	private float timeMonsterAppearing    = 0.6f;
	private float timeMonsterFlickering   = 2.0f;
	private float timeMonsterDisappearing = 1.0f;

	[SerializeField] private float timebetweenFlicker = 0.1f;
	[SerializeField] private float offFlickerTime = 0.1f;
	private float elapsedFlickerTime = 0.0f;

	private enum State
	{
		VISIBLE = 0,
		FLICKERING = 1,
		MONSTER_APPEARING = 2,
		MONSTER_FLICKERING = 3,
		MONSTER_DISAPPAERING = 4
	}

	State currentState = State.VISIBLE;
	
	// Update is called once per frame
	void Update () 
	{
		if ( ! photonView.isMine ) return;

		elapsedTime += Time.deltaTime;
		switch ( currentState )	
		{
			case State.VISIBLE:
				if ( elapsedTime >= timeVisible )
				{
					currentState = State.FLICKERING;
					elapsedTime = elapsedTime % timeVisible;
					elapsedFlickerTime = 0.0f;

					lightObject.enabled = false;
					monsterObject.SetRenderChild( false );
				}
				break;
			case State.FLICKERING:
				if ( elapsedTime >= timeFlickering )
				{
					currentState = State.MONSTER_APPEARING;
					elapsedTime = elapsedTime % timeFlickering;
					elapsedFlickerTime = 0.0f;

					lightObject.enabled = false;
					monsterObject.SetRenderChild( true );
				}
				else
				{
					elapsedFlickerTime += Time.deltaTime;
					if ( lightObject.enabled && elapsedFlickerTime >= timebetweenFlicker )
					{
						elapsedFlickerTime = elapsedFlickerTime % timebetweenFlicker;
						lightObject.enabled = false;
					}
					else if ( ! lightObject.enabled && elapsedFlickerTime >= offFlickerTime )
					{
						elapsedFlickerTime = elapsedFlickerTime % offFlickerTime;
						lightObject.enabled = true;
					}
				}
				break;
			case State.MONSTER_APPEARING:
				if ( elapsedTime >= timeMonsterAppearing )
				{
					currentState = State.MONSTER_FLICKERING;
					elapsedTime = elapsedTime % timeMonsterAppearing;
					elapsedFlickerTime = 0.0f;
					lightObject.enabled = true;
					monsterObject.SetRenderChild( true );
				}
				break;
			case State.MONSTER_FLICKERING:
				if ( elapsedTime >= timeMonsterFlickering )
				{
					currentState = State.MONSTER_DISAPPAERING;
					elapsedTime = elapsedTime % timeMonsterFlickering;
					elapsedFlickerTime = 0.0f;

					lightObject.enabled = false;
					monsterObject.SetRenderChild( false );
				}
				else
				{					
					elapsedFlickerTime += Time.deltaTime;
					if ( lightObject.enabled && elapsedFlickerTime >= timebetweenFlicker )
					{
						elapsedFlickerTime = elapsedFlickerTime % timebetweenFlicker;
						lightObject.enabled = false;
					}
					else if ( ! lightObject.enabled && elapsedFlickerTime >= offFlickerTime )
					{
						elapsedFlickerTime = elapsedFlickerTime % offFlickerTime;
						lightObject.enabled = true;
					}
				}
				break;
			case State.MONSTER_DISAPPAERING:
				if ( elapsedTime >= timeMonsterDisappearing )
				{
					currentState = State.VISIBLE;
					elapsedTime = elapsedTime % timeMonsterDisappearing;
					elapsedFlickerTime = 0.0f;
					lightObject.enabled = true;
					monsterObject.SetRenderChild( false );
				}
				break;
		}
	}
}
