﻿using UnityEngine;
using System.Collections;

// This Script makes sure the enabled/disabled state of the renderChild object ( the child object of the moster that contains all rendering into )
public class MonsterHandler : Photon.MonoBehaviour 
{
	[SerializeField] private GameObject renderChild = null;
	private MonsterStateMachine stateMachine;

	void OnEnable()
	{
		stateMachine = GetComponent<MonsterStateMachine>();
	}

	public void SetRenderChild ( bool enabled )
	{
		renderChild.SetActive( enabled );
	}

	void OnPhotonSerializeView( PhotonStream stream, PhotonMessageInfo info) 
	{
		// Send Upates through the network
		if ( stream.isWriting )
		{
			stream.SendNext( renderChild.activeSelf );
		}
		else // reading
		{
			renderChild.SetActive( (bool) stream.ReceiveNext() );
		}
	}

	// When the Ground Player Joins or Leaves the Game, then mark him for spooks
	// 
}
