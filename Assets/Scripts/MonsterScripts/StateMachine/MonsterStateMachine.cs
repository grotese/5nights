﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MonsterState : byte
{
	PATROLLING = 0,
	ALERTED
}

[System.Serializable]
public struct StateBehavior
{
	public MonsterState state;
	public BaseMonsterState behaviour;
}

[DisallowMultipleComponent]
public class MonsterStateMachine : Photon.MonoBehaviour 
{
#region Serialzed Fields

	[SerializeField] private MonsterState currentState = MonsterState.PATROLLING;
	[SerializeField] private StateBehavior[] states = new StateBehavior[0];

#endregion

#region Monster Data

	public float visionRange = 10.0f;
	[HideInInspector] public Transform groundPlayer;
	public MonsterState state { get { return currentState; } }

#endregion

#region Private Fields

	private BaseMonsterState currentStateBehavior = null;
	private bool isApplicationShuttingDown = false;

#endregion

#region Monomethods

	protected virtual void Awake()
	{
		// Disable all my states, so they don't run that still OnEnable when they aren't even enabled
		foreach ( var state in states )
		{
			// Skip if that's our initial state
			if ( state.state == currentState )
			{
				state.behaviour.enabled = true;   // enable initial state
				currentStateBehavior = state.behaviour;
				continue;
			}

			state.behaviour.enabled = false;   // disable otherwise
		}

		// Error out if the starting state they specified doesn't exist
		if ( ! currentStateBehavior )
			Debug.LogError("Starting State: " + currentState.ToString() + " Not Found! ");
	}

	protected virtual void OnEnable()
	{
		// If we were enabled and we are in a room, then we can start
		if ( PhotonNetwork.inRoom )
		{
			currentStateBehavior.onStateEnter();
		}
	}

	protected virtual void OnDisable()
	{
		// Unity is dumb and will OnDisable AFTER OnApplicationQuit, this guarentees this doesn't happen
		if ( ! isApplicationShuttingDown )
		{
			currentStateBehavior.onStateExit();
		}
	}

	protected virtual void OnApplicationQuit()
	{
		isApplicationShuttingDown = true;		
	}

#endregion

#region Helper Methods

	/// <summary>
	/// Return monobehavior of the given state.
	/// returns null if not found
	/// </summary>
	protected BaseMonsterState getStateBehavior( MonsterState state )
	{
		StateBehavior state_behavior = Array.Find( 
			states,
			sb => sb.state == state 
		);

		return state_behavior.behaviour;
	}

#endregion

#region Public Methods

	public void changeState( MonsterState new_state )
	{
		if ( PhotonNetwork.inRoom )
			photonView.RPC( "changeState_RPC", PhotonTargets.All, (byte) new_state );    // Tell my local version, and all remote versions that it's flicking time
	}

#endregion

#region Network Junk

	[PunRPC]
	void changeState_RPC( byte byte_new_state )
	{
		MonsterState new_state = (MonsterState) byte_new_state;

		if ( currentStateBehavior )
		{
			photonView.ObservedComponents.Remove( currentStateBehavior );   // Remove State from Observed List
			currentStateBehavior.onStateExit();
			currentStateBehavior.enabled = false;
		}

		currentState = new_state;
		currentStateBehavior = getStateBehavior( new_state );

		// If the found behavior isn't null, then enable it
		if ( currentStateBehavior )
		{
			currentStateBehavior.enabled = true;
			currentStateBehavior.onStateEnter();
			photonView.ObservedComponents.Add( currentStateBehavior );   // Add State to Observed List
		}
	}

	void OnJoinedRoom()
	{
		// If we joined a room, run the current state's behavior
		currentStateBehavior.onStateEnter();
	}

#endregion

}
