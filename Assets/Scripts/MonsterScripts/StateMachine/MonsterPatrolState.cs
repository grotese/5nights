﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

enum MonsterMovingState
{
	IDLE,
	PRPARING_TO_MOVE,
	MOVING
}

[RequireComponent(typeof(MonsterStateMachine))]
public class MonsterPatrolState : BaseMonsterState
{
	public PathNodeInfo startingNode     = null;
	public MonsterHandler monsterHandler = null;

	private PathNodeInfo currentNode = null;
	private PathNodeInfo prevNode    = null;
	private PathNodeInfo nextNode    = null;

	private MonsterMovingState movingState = MonsterMovingState.IDLE;

	public float elapsedTime = 0.0f;

	[SerializeField] private float waitTime = 5.0f;
	[SerializeField] private float flickerTime = 2.0f;
	[SerializeField] private float monsterTravelTime = 0.5f;
	[SerializeField] private MonsterAudioHandler audioHandler = null;

	private IEnumerator watchPlayerCoroutine;

#region Monomethods

	public override void onStateEnter()
	{
		if ( photonView.isMine )
		{
			// If we haven't even looked at the starting node yet, then set the current node to the starting node and move to it
			if ( currentNode == null )
			{
				currentNode = startingNode;
				MoveMonsterToCurrentNode();
			}

			movingState = MonsterMovingState.IDLE;    // Start Idle

			// Start Coroutines
			StartCoroutine( waitToMove() );
			// The player entered my sphere, but I don't know if I can see him yet
			StartCoroutine( watchPlayerCoroutine = watchPlayer() );
		}
	}

	public override void onStateExit()
	{
		StopAllCoroutines();
		watchPlayerCoroutine = null;

		// Reset Lights and Shit
		if ( currentNode )
		{
			// Turn on the lights
			foreach ( ILightSource light_source in currentNode.lights )
			{
				light_source.lightState = LightState.ON;
			}

			// Also enable the cameras on both nodes
			foreach ( ISecurityCamera security_camera in currentNode.cameras )
			{
				security_camera.securityCameraState = SecurityCameraState.ON;
			}
		}

		if ( nextNode )
		{
			foreach ( ILightSource light_source in nextNode.lights )
			{
				light_source.lightState = LightState.ON;
			}
			foreach ( ISecurityCamera security_camera in nextNode.cameras )
			{
				security_camera.securityCameraState = SecurityCameraState.ON;
			}
		}

		// Turn the Monster Visible
		monsterHandler.SetRenderChild(true);
	}


#endregion Monomethods

#region Helper Methods

	private void MoveMonsterToCurrentNode()
	{
		if ( currentNode != null && monsterHandler != null )
		{
			//Debug.Log("Switching to New Node");
			monsterHandler.GetComponent<Transform>().position = currentNode.GetComponent<Transform>().position;
			monsterHandler.GetComponent<Transform>().rotation = currentNode.GetComponent<Transform>().rotation;
		}
	}

	PathNodeInfo findNextPathNode()
	{
		// Move to Next Available Node
		PathNodeInfo next_node;

		int next_node_index = Random.Range( 0, currentNode.connectedPathNodes.Count );

		next_node = currentNode.connectedPathNodes[ next_node_index ];

		// If rand just gave us the previous node, but we have other options, then grab the next one
		if ( next_node == prevNode && currentNode.connectedPathNodes.Count > 1 )
		{
			next_node_index = ( next_node_index + 1 )  % currentNode.connectedPathNodes.Count;    // Rand, you had your chance, but ya fucked it up
			next_node = currentNode.connectedPathNodes[ next_node_index ];
		}

		return next_node;
	}

#endregion

#region Corouitines

	// The player entered my sphere, but I don't know if I can see him yet
	// Poll until I can see him, then POUNCE!
	IEnumerator watchPlayer()
	{
		// Create Layer Mask to Ignore myself
		int layerMask = 1 << LayerMask.NameToLayer("Monster");
		layerMask = ~layerMask;

		while ( true )
		{
			yield return new WaitForSeconds( 0.5f );    // Delay so I'm not doing a collision ray case every frame

			if ( GlobalGameData.groundPlayer == null ) continue;

			Transform player_trans = GlobalGameData.groundPlayer.transform;
			RaycastHit hit_info;
			Vector3 position_difference = player_trans.position - this.transform.position;

			if ( Physics.Raycast( 
					this.transform.position,             // Start from the monster's position
					position_difference,                 // direction = destination - source
					out hit_info,                        // pass in hit info
					stateMachine.visionRange,            // max distance is total distance between monster and player
					layerMask )                          // Layermask to ignore self
				 && hit_info.collider.tag == "Player" )  // And the person we are looking at is the player  
			{
				// Trigger some state change to murderlize him
				stateMachine.changeState(MonsterState.ALERTED);
				break;
			}
		}
	}

	IEnumerator waitToMove()
	{
		movingState = MonsterMovingState.IDLE;

		yield return new WaitForSeconds( waitTime );    // Wait before we move

		// Find Next Node
		nextNode = findNextPathNode();

		// Begin dual light flickering
		foreach ( ILightSource light_source in currentNode.lights )
		{
			light_source.lightState = LightState.FLICKERING;
		}

		foreach ( ILightSource light_source in nextNode.lights )
		{
			light_source.lightState = LightState.FLICKERING;
		}

		yield return StartCoroutine( waitForFlickeringToEnd() );
	}

	IEnumerator waitForFlickeringToEnd()
	{
		movingState = MonsterMovingState.PRPARING_TO_MOVE;
		yield return new WaitForSeconds( flickerTime );

		// Turn off the Lights
		foreach ( ILightSource light_source in currentNode.lights )
		{
			light_source.lightState = LightState.OFF;
		}

		foreach ( ILightSource light_source in nextNode.lights )
		{
			light_source.lightState = LightState.OFF;
		}

		// Turn the Monster Invisible
		monsterHandler.SetRenderChild(false);

		// Also disable the cameras on both nodes
		foreach ( ISecurityCamera security_camera in currentNode.cameras )
		{
			security_camera.securityCameraState = SecurityCameraState.OFF;
		}

		foreach ( ISecurityCamera security_camera in nextNode.cameras )
		{
			security_camera.securityCameraState = SecurityCameraState.OFF;
		}

		audioHandler.PlayMovementSound();

		// Wait for the Monster to get its fatass over
		yield return StartCoroutine( waitForMonsterMove() );
	}

	IEnumerator waitForMonsterMove()
	{
		movingState = MonsterMovingState.MOVING;
		yield return new WaitForSeconds( monsterTravelTime );

		// Turn on the lights
		foreach ( ILightSource light_source in currentNode.lights )
		{
			light_source.lightState = LightState.ON;
		}

		foreach ( ILightSource light_source in nextNode.lights )
		{
			light_source.lightState = LightState.ON;
		}

		// Also enable the cameras on both nodes
		foreach ( ISecurityCamera security_camera in currentNode.cameras )
		{
			security_camera.securityCameraState = SecurityCameraState.ON;
		}

		foreach ( ISecurityCamera security_camera in nextNode.cameras )
		{
			security_camera.securityCameraState = SecurityCameraState.ON;
		}

		// Move the Monster
		prevNode = currentNode;
		currentNode = nextNode;
		MoveMonsterToCurrentNode();

		// Turn the Monster Visible
		monsterHandler.SetRenderChild(true);

		yield return StartCoroutine( waitToMove() );
	}

#endregion

#region Network Junk

	void OnPhotonSerializeView( PhotonStream stream, PhotonMessageInfo info) 
	{
		// Send Upates through the network
		if ( stream.isWriting )
		{
			stream.SendNext( transform.rotation );
		}
		else // reading
		{
			transform.rotation = (Quaternion) stream.ReceiveNext();
		}
	}

#endregion

}

