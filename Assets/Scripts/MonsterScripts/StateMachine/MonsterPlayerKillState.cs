﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MonsterStateMachine))]
public class MonsterPlayerKillState : BaseMonsterState
{
	[SerializeField] private MonsterAudioHandler audio = null;
	[SerializeField] private float timeToKillPlayer = 1.0f;

	private IEnumerator watchPlayerCoroutine;
	private IEnumerator lookAtPlayerCoroutine;
	private IEnumerator countdownToKillPlayerCoroutine;

	public override void onStateEnter()
	{
		// Get that son of a bitch
		if ( GlobalGameData.groundPlayer == null )
		{
			Debug.LogWarning("Swtiched to Kill Player, but the ground player isn't set ... shit's gunna break");
			return;
		}

		if ( watchPlayerCoroutine != null )
			StopCoroutine( watchPlayerCoroutine );

		// Only Switch to behavior that can change state if you are MY MONSTER!
		if ( photonView.isMine )
		{
			StartCoroutine( watchPlayerCoroutine = watchPlayer() );
			StartCoroutine( countdownToKillPlayerCoroutine = countdownToKillPlayer() );
		}

		StartCoroutine( lookAtPlayerCoroutine = lookAtPlayer() );
		audio.PlayAlertSound();
	}

	public override void onStateExit()
	{
		StopAllCoroutines();
		watchPlayerCoroutine = null;
		lookAtPlayerCoroutine = null;
		countdownToKillPlayerCoroutine = null;
	}

	IEnumerator countdownToKillPlayer()
	{
		yield return new WaitForSeconds( timeToKillPlayer );

		// Send Player a message that they are now fucking dead
		audio.PlayKillSound();
		GlobalGameData.groundPlayer.BroadcastMessage("KillPlayer", SendMessageOptions.DontRequireReceiver );

		// Our work here is done
	}

	IEnumerator lookAtPlayer()
	{
		while ( true )
		{
			// yield return new WaitForSeconds( 0.2f );
			yield return null;
			if ( GlobalGameData.groundPlayer == null ) continue;

			Vector3 player_position = GlobalGameData.groundPlayer.transform.position;
			player_position.y = this.transform.position.y;

			this.transform.LookAt( player_position );
		}
	}

	// Watch the player until we can no longer see him
	// TODO: program monster object permanence - it's like I'm programming a baby :3
	IEnumerator watchPlayer()
	{
		// Create Layer Mask to Ignore myself
		int layerMask = 1 << LayerMask.NameToLayer("Monster");
		layerMask = ~layerMask;

		while ( true )
		{
			yield return new WaitForSeconds( 0.5f );    // Delay so I'm not doing a collision ray case every frame

			if ( GlobalGameData.groundPlayer == null )
			{
				// If the player has left our sphere or vision, then exit this state
				stateMachine.changeState(MonsterState.PATROLLING);
				break;
			}

			Transform player_trans = GlobalGameData.groundPlayer.transform;
			RaycastHit hit_info;
			Vector3 position_difference = player_trans.position - this.transform.position;

			if ( ! Physics.Raycast( 
					this.transform.position,             // Start from the monster's position
					position_difference,                 // direction = destination - source
					out hit_info,                        // pass in hit info
					stateMachine.visionRange,            // max distance is total distance between monster and player
					layerMask )                          // Layermask to ignore self
				 || hit_info.collider.tag != "Player" )  // And the person we are looking at is the player  
			{
				// If the player has left our sphere or vision, then exit this state
				stateMachine.changeState(MonsterState.PATROLLING);
				break;
			}
		}
	}

#region Network Junk

	void OnPhotonSerializeView( PhotonStream stream, PhotonMessageInfo info) 
	{
		// Send Upates through the network
		// if ( stream.isWriting )
		// {
		// 	// stream.SendNext( transform.rotation );
		// }
		// else // reading
		// {
		// 	// transform.rotation = (Quaternion) stream.ReceiveNext();
		// }
	}

#endregion
}
