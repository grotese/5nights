﻿using UnityEngine;
using System.Collections;

public interface IMonsterState
{
	void onStateEnter();
	void onStateExit();
}
