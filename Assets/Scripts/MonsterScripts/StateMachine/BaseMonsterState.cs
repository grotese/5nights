﻿using UnityEngine;
using System.Collections;

public class BaseMonsterState : Photon.MonoBehaviour, IMonsterState
{
#region Serialized Fields

	[SerializeField] protected MonsterStateMachine stateMachine = null;

#endregion

#region IMonsterState

	/// <summary>
	/// Method is called when State starts, or, if the state is the starting state, when the room is joined!
	/// </summary>
	public virtual void onStateEnter(){}

	/// <summary>
	/// Method is called when state exits, NOT when the application or scene is shutting down
	/// </summary>
	public virtual void onStateExit(){}

#endregion

#region Monobehavior Methods

	protected virtual void Reset()
	{
		this.enabled = false;    // disable myself
	}

	protected virtual void OnEnable(){}
	protected virtual void OnDisable(){}

#endregion
}
