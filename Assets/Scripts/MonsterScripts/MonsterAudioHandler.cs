﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PhotonView))]
public class MonsterAudioHandler : MonoBehaviour 
{

#region Audio Clips

	[SerializeField] private AudioSource audioSource = null;
	[SerializeField] private AudioSource killAudioSource = null;
	[SerializeField] private AudioClip movementClip = null;
	[SerializeField] private AudioClip killSound = null;
	[SerializeField] private AudioClip alertSound = null;

#endregion

#region Private Methods

	[PunRPC]
	void PlayMovementSound_RPC()
	{
		if ( audioSource.isPlaying )
		{
			audioSource.Stop();
		}

		audioSource.clip = movementClip;
		audioSource.Play();
	}

#endregion

#region Public Methods
	
	public void PlayMovementSound()
	{
		this.GetComponent<PhotonView>().RPC( "PlayMovementSound_RPC", PhotonTargets.All );
	}

	public void PlayKillSound()
	{
		if ( killAudioSource.isPlaying )
		{
			killAudioSource.Stop();
		}

		killAudioSource.clip = killSound;
		killAudioSource.Play();	
	}

	public void PlayAlertSound()
	{
		if ( killAudioSource.isPlaying )
		{
			killAudioSource.Stop();
		}

		killAudioSource.clip = alertSound;
		killAudioSource.Play();	
	}

#endregion
}
