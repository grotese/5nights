﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

// public struct TransformLite
// {
// 	public Vector3 position;
// 	public Quaternion rotation;
// 	public Vector3 scale;

// 	public TransformLite( Transform transform )
// 	{
// 		this.position = transform.position;
// 		this.rotation = transform.rotation;
// 		this.scale    = transform.scale;
// 	}
// }

// This Script will allow the Camera Player to lean around the security screen to look at the door way
// Because this handles input, it should obviously only be enabled on the Local Host
public class CameraPlayerLean : MonoBehaviour 
{
	[TooltipAttribute("Camera Transform to Move, when the Player wants to lean")]
	[SerializeField] private Transform cameraTransform = null;

	[TooltipAttribute("Lean Target. Where to lean to.")]
	[SerializeField] private Transform leanTarget = null;
	[SerializeField] private Transform turnleftTarget = null;

	[SerializeField] private float smoothing = 7.0f;

	// The Starting position of the CameraTransform
	[SerializeField] private Transform startingTransform = null;

	[SerializeField] private DepthOfField depthOfField = null;
	[SerializeField] private float nearFocalDistance = 2.5f;
	[SerializeField] private float farFocalDistance = 10.0f;

	internal enum CameraPlayerState : byte
	{
		CENTERED = 0,
		LEANING_RIGHT,
		TURNED_LEFT
	}

	private CameraPlayerState cameraPlayerState = CameraPlayerState.CENTERED;

	private IEnumerator leanEnumerator = null;

	// Use this for initialization
	void OnEnable() 
	{
		depthOfField.focalLength = nearFocalDistance;    // Set Focal Distance to the close one
	}

	// Update is called once per frame
	void Update()
	{
		switch ( cameraPlayerState )
		{
			case CameraPlayerState.CENTERED:

				// Check to see if we should start leaning right
				if ( Input.GetAxis("Horizontal") > 0.0f )
				{
					if ( leanEnumerator != null )
					{
						StopCoroutine(leanEnumerator);
						leanEnumerator = null;
					}

					StartCoroutine( leanEnumerator = LeanCoroutine( leanTarget, farFocalDistance ) );

					cameraPlayerState = CameraPlayerState.LEANING_RIGHT;
				}
				// Check to see if we should start turning left
				else if ( Input.GetAxis("Horizontal") < 0.0f )
				{
					if ( leanEnumerator != null )
					{
						StopCoroutine(leanEnumerator);
						leanEnumerator = null;
					}

					StartCoroutine( leanEnumerator = LeanCoroutine( turnleftTarget, nearFocalDistance ) );

					cameraPlayerState = CameraPlayerState.TURNED_LEFT;
				}
				break;
			case CameraPlayerState.LEANING_RIGHT:
				if ( Input.GetAxis("Horizontal") <= 0.0f )
				{
					if ( leanEnumerator != null )
					{
						StopCoroutine(leanEnumerator);
						leanEnumerator = null;
					}

					StartCoroutine( leanEnumerator = LeanCoroutine( startingTransform, nearFocalDistance ) );

					cameraPlayerState = CameraPlayerState.CENTERED;
				}
				break;
			case CameraPlayerState.TURNED_LEFT:
				if ( Input.GetAxis("Horizontal") >= 0.0f )
				{
					if ( leanEnumerator != null )
					{
						StopCoroutine(leanEnumerator);
						leanEnumerator = null;
					}

					StartCoroutine( leanEnumerator = LeanCoroutine( startingTransform, nearFocalDistance ) );

					cameraPlayerState = CameraPlayerState.CENTERED;
				}
				break;
		}
	}

	private IEnumerator LeanCoroutine( Transform destination, float focal_target )
	{
		while ( Vector3.Distance( cameraTransform.position, destination.position ) > 0.01f ||
		        Mathf.Abs( Quaternion.Angle( cameraTransform.rotation, destination.rotation ) ) > 0.01f )
		{
			cameraTransform.position = Vector3.Lerp( cameraTransform.position, destination.position, smoothing * Time.deltaTime );
			cameraTransform.rotation = Quaternion.Lerp( cameraTransform.rotation, destination.rotation, smoothing * Time.deltaTime );
			depthOfField.focalLength = Mathf.Lerp( depthOfField.focalLength, focal_target, smoothing * Time.deltaTime );
			yield return null;
		}

		cameraTransform.position = destination.position;  // Finish it
		cameraTransform.rotation = destination.rotation;
		depthOfField.focalLength = focal_target;
	}
}
