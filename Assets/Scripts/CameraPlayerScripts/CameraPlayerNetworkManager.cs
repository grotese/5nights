﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Used to Sync Information for the CameraPlayer
public class CameraPlayerNetworkManager : Photon.MonoBehaviour 
{
	[TooltipAttribute("Components to disable if remote, or keep enabled if local")]
	[SerializeField] private List< Behaviour > localLogicComponents = new List< Behaviour >();

	[TooltipAttribute( "Components to monitor for 'enable' field state changes" )]
	[SerializeField] private Behaviour[] syncEnabledStateComponents = new Behaviour[0];

	// Use this for initialization
	void Start () 
	{
		if ( photonView.isMine )
		{
			foreach ( Behaviour logic in localLogicComponents )
			{
				logic.enabled = true;
			}
			
			RegisterSelfWithCameraSystem();
		}
		else
		{
			foreach ( Behaviour logic in localLogicComponents )
			{
				logic.enabled = false;
			}
		}

		GlobalGameData.cameraPlayer = this.gameObject;
	}

	void OnDestroy()
	{
		GlobalGameData.cameraPlayer = null;
	}

	void RegisterSelfWithCameraSystem()
	{
		GameObject cameraSystem = GlobalGameData.cameraSystem;

		Debug.Assert( cameraSystem != null, "Camera Player Failed to find Camera System in this Scene");

		MinimapHandler minimapHandler = cameraSystem.GetComponent<MinimapHandler>();

		Debug.Assert( minimapHandler != null, "Camera Player Failed to find minimapHandler in this Scene");

		// Get Camera Component of the Child of the 
		Camera playerCamera = this.GetComponentInChildren<Camera>();    // get the Render Camera from the Camera Player

		if ( playerCamera != null )
		{
			minimapHandler.setUICamera( playerCamera );
		}
		else
		{
			Debug.LogWarning("Camera Player Doesn't Have Camera Object!");
		}
	}
	
	void OnPhotonSerializeView( PhotonStream stream, PhotonMessageInfo info) 
	{
		// Send Upates through the network
		if ( stream.isWriting )
		{
			foreach ( Behaviour components in syncEnabledStateComponents )
			{
				stream.SendNext( components.enabled );
			}
		}
		else // reading
		{
			foreach ( Behaviour components in syncEnabledStateComponents )
			{
				components.enabled = (bool) stream.ReceiveNext();
			}
		}
	}
}
