﻿using UnityEngine;
using UnityEngine.UI;

public class BrightnessSliderHandler : MonoBehaviour 
{
	[SerializeField] Color ambientDarkest = new Color( 1, 1, 1, 0 );
	[SerializeField] Color ambientLightest = new Color( 1, 1, 1, 0.3f );

	[SerializeField] Image imageOverlay = null;

	public void OnBrightnessSliderChange( float slider_value )
	{
		imageOverlay.color = Color.Lerp( ambientDarkest, ambientLightest, slider_value );
	}
}
