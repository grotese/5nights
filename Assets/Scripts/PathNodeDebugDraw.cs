﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(PathNodeInfo))]
public class PathNodeDebugDraw : MonoBehaviour 
{
	void Update()
	{
		Vector3 position       = this.GetComponent<Transform>().position;
		PathNodeInfo node_info = this.GetComponent<PathNodeInfo>();

		foreach ( PathNodeInfo path_node in node_info.connectedPathNodes )
		{
			if ( path_node != null )
				Debug.DrawLine( position, path_node.GetComponent<Transform>().position, Color.green );
		}
	}
}
