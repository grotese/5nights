﻿
public interface ISealableDoor 
{
	bool isDoorOpen { get; }
	void openDoor();
	void closeDoor();
	void toggleDoor();
}
