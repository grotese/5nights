﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class DeathScreenFade : MonoBehaviour 
{
	[SerializeField] private float fadeSpeed = 1.0f;
	[SerializeField] private GameObject deathMovie = null;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine( fadeToBlack() );
	}
	
	IEnumerator fadeToBlack()
	{
		while( 
			this.GetComponent<Camera>().backgroundColor.r > 0.05f || 
			this.GetComponent<Camera>().backgroundColor.g > 0.05f || 
			this.GetComponent<Camera>().backgroundColor.b > 0.05f )
		{
			this.GetComponent<Camera>().backgroundColor = Color.Lerp(
				this.GetComponent<Camera>().backgroundColor, 
				Color.black, 
				Time.deltaTime * fadeSpeed
			);

			yield return null;
		}

		deathMovie.SetActive(true);
	}
}
