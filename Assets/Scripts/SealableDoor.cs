﻿using UnityEngine;
// using System.Collections;

[RequireComponent(typeof(PhotonView))]
public class SealableDoor : MonoBehaviour, ISealableDoor 
{
	[SerializeField] private Animation doorAnimation = null;

	void Start()
	{
		if ( isDoorOpen )
		{
			doorAnimation.PlayQueued("open");
		}
		// Defaults to closed, so I don't need to open it again
	}

#region RPC Calls

	[PunRPC]
	private void openDoor_RPC()
	{
		doorAnimation.PlayQueued("open");
		isDoorOpen = true;
	}

	[PunRPC]
	private void closeDoor_RPC()
	{
		doorAnimation.PlayQueued("close");
		isDoorOpen = false;
	}

#endregion

#region ISealableDoor 

	[SerializeField] protected bool _isDoorOpen = false;
	public bool isDoorOpen 
	{ 
		get { return this._isDoorOpen; }
		protected set { _isDoorOpen = value; }
	}

	public void openDoor()
	{
		this.GetComponent<PhotonView>().RPC( "openDoor_RPC", PhotonTargets.All );
	}

	public void closeDoor()
	{
		this.GetComponent<PhotonView>().RPC( "closeDoor_RPC", PhotonTargets.All );
	}

	public void toggleDoor()
	{
		if ( isDoorOpen )
		{
			closeDoor();
		}
		else
		{
			openDoor();
		}
	}

#endregion
}
