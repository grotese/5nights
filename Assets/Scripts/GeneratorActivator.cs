﻿using UnityEngine;
using System.Collections;

public class GeneratorActivator : MonoBehaviour 
{
	[SerializeField] private float chargeRate = 5.0f;

	public void StartActivation()
	{
		StartCoroutine(chargePower());
	}

	public void StopActivation()
	{
		StopAllCoroutines();
	}

	IEnumerator chargePower()
	{
		while ( true )
		{
			if ( GlobalGameData.powerResource )
			{
				GlobalGameData.powerResource.modifyPowerLevel(  chargeRate * Time.deltaTime ); 
			}

			yield return null;
		}
	}
}
