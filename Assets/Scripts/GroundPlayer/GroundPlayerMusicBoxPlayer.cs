using UnityEngine;
using System.Collections;

public class GroundPlayerMusicBoxPlayer : Photon.MonoBehaviour  
{
	[SerializeField] private AudioSource audioSource = null;
	[SerializeField] private KeyCode musicBoxKey = KeyCode.F;

	public delegate void MusicBoxPlaying( bool playing );
	public event MusicBoxPlaying OnMusicBoxPlaying = delegate{};

	void OnEnable()
	{
		if ( ! photonView.isMine ) return;

		// Start the Coroutines
		StartCoroutine(waitForPlayerInput());
	}

	void OnDisable()
	{
		// Stop all Coroutines
		StopAllCoroutines();
	}

	IEnumerator waitForPlayerInput()
	{
		// Wait for the pLayer to press and hold the 'E' key
		while ( ! Input.GetKey( musicBoxKey ) )
		{
			yield return null;
		}

		// Start Playing the Music Box
		photonView.RPC( "startPlayerMusicBox_RPC", PhotonTargets.All );

		yield return StartCoroutine( waitForPlayerStopPlaying() );
	}

	IEnumerator waitForPlayerStopPlaying()
	{
		// Wait for the Music Box Key to be released
		while ( Input.GetKey( musicBoxKey ) )
		{
			yield return null;
		}

		// Stop Playing the Music Box
		photonView.RPC( "stopPlayerMusicBox_RPC", PhotonTargets.All );

		yield return StartCoroutine( waitForPlayerInput() );	
	}

#region Music Box

	public bool isPlaying
	{
		get { return audioSource.isPlaying; }
	}

	[PunRPC]
	void startPlayerMusicBox_RPC()
	{
		audioSource.Play();
		OnMusicBoxPlaying( true );
	}

	[PunRPC]
	void stopPlayerMusicBox_RPC()
	{
		audioSource.Stop();
		OnMusicBoxPlaying( false );
	}

#endregion
}
