﻿using UnityEngine;
using System;
using System.Collections;

// Handles when the player looks at something at can activate it
public class GroundPlayerActivator : MonoBehaviour 
{
	[SerializeField] private float maxActivatorDistance = 5.0f;
	[SerializeField] private Transform cameraTransform = null;
	[SerializeField] private KeyCode activateKeyCode = KeyCode.Space;
	[HideInInspector] public bool charging = false;

	private WeakReference targetActivator;

	public GeneratorActivator currentActivator
	{
		get
		{
			if ( targetActivator != null )
				return targetActivator.Target as GeneratorActivator;
			return null;
		}
	}

	void Start()
	{
		StartCoroutine(checkForActiveButtonDown());
	}

	void Update()
	{
		Vector3 forward = cameraTransform.TransformDirection( Vector3.forward );    // Transform the Forward vector onto the camera's transform, so it will be forward, according to the camera position

		RaycastHit hit_info = new RaycastHit();

		if ( Physics.Raycast( cameraTransform.position, forward, out hit_info, maxActivatorDistance ) )
		{
			Debug.DrawLine( cameraTransform.position, cameraTransform.position + maxActivatorDistance * forward, Color.red );
			GeneratorActivator activator = hit_info.collider.GetComponent<GeneratorActivator>();

			if ( activator )
			{
				targetActivator = new WeakReference( activator );
			}
		}
		else
		{
			if ( targetActivator != null && targetActivator.Target != null )
			{
				( targetActivator.Target as GeneratorActivator ).StopActivation();
			}

			targetActivator = null;
			Debug.DrawLine( cameraTransform.position, cameraTransform.position + maxActivatorDistance * forward, Color.blue );
		}
	}

	IEnumerator checkForActiveButtonDown()
	{
		while ( true )
		{
			// Only check, if the player is over an activator
			if ( targetActivator != null && targetActivator.Target != null )
			{
				if ( Input.GetKey( activateKeyCode ) )
				{
					( targetActivator.Target as GeneratorActivator ).StartActivation();
					break;
				}
			}

			yield return null;
		}

		yield return StartCoroutine( holdDownCharge() );
	}

	IEnumerator holdDownCharge()
	{
		charging = true;

		while ( targetActivator != null && targetActivator.Target != null && Input.GetKey( activateKeyCode ) )
		{
			yield return null;
		}

		if ( targetActivator != null && targetActivator.Target != null )
		{
			( targetActivator.Target as GeneratorActivator ).StopActivation();
		}

		charging = false;

		yield return StartCoroutine( checkForActiveButtonDown() );
	}
}
