﻿using UnityEngine;
using System.Collections;

// This behavior will enable/disable components of the player, depending on if the player is a local instance, or remote instance
public class PlayerNetworkHandler : Photon.MonoBehaviour 
{
	private Vector3 position;
	private Quaternion rotation = Quaternion.identity;
	private float smoothing = 10.0f;

	[SerializeField] private GameObject groundPlayerUICanvasPrefab = null;
	private GameObject groundPlayerUICanvasInstance = null;

	// Use this for initialization
	void Start () 
	{
		if ( photonView.isMine )
		{
			// Enable Components that only the local version should have ( logic, first person models, physics, etc )
			this.GetComponent<GroundPlayerMovement>().enabled = true;
			this.GetComponentInChildren<Camera>().enabled = true;
			this.GetComponentInChildren<AudioListener>().enabled = true;

			// Disable other components, like full 3D models, animations, etc
			//this.GetComponent<MeshRenderer>().enabled = false;
		}
		else
		{
			// diaable Components that only the local version should have ( logic, first person models, physics, etc )
			this.GetComponent<GroundPlayerMovement>().enabled = false;
			this.GetComponentInChildren<Camera>().enabled = false;
			this.GetComponentInChildren<AudioListener>().enabled = false;

			// enabled other components, like full 3D models, animations, etc
			this.GetComponent<MeshRenderer>().enabled = true;

			StartCoroutine("UpdateData");
		}
	}
	
	void OnEnable()
	{
		// I am born, tell the world
		GlobalGameData.groundPlayer = this.gameObject;

		if ( photonView.isMine )
		{
			groundPlayerUICanvasInstance = Instantiate(groundPlayerUICanvasPrefab) as GameObject;
			GroundPlayerUI ui = groundPlayerUICanvasInstance.GetComponent<GroundPlayerUI>();
			ui.musicPlayer = this.GetComponent<GroundPlayerMusicBoxPlayer>();
			ui.activator = this.GetComponent<GroundPlayerActivator>();
		}
	}

	void OnDisable()
	{
		if ( photonView.isMine && groundPlayerUICanvasPrefab != null )
		{
			Destroy( groundPlayerUICanvasInstance );
		}

		// erase myself from the global game data table
		GlobalGameData.groundPlayer = null;
	}

	private IEnumerator UpdateData()
	{
		while ( true )
		{
			GetComponent<Transform>().position = Vector3.Lerp   ( GetComponent<Transform>().position, position, Time.deltaTime * smoothing );
			GetComponent<Transform>().rotation = Quaternion.Lerp( GetComponent<Transform>().rotation, rotation, Time.deltaTime * smoothing );
			yield return null;
		}
	}

	void OnPhotonSerializeView( PhotonStream stream, PhotonMessageInfo info) 
	{
		// Send Upates through the network
		if ( stream.isWriting )
		{
			stream.SendNext( GetComponent<Transform>().position );
			stream.SendNext( GetComponent<Transform>().rotation );
		}
		else // reading
		{
			position = (Vector3)    stream.ReceiveNext();
			rotation = (Quaternion) stream.ReceiveNext();
		}
	}
}
