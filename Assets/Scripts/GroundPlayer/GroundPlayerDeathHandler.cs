﻿using UnityEngine;

public class GroundPlayerDeathHandler : Photon.MonoBehaviour 
{
	[SerializeField] private GameObject deathScreenPrefab = null;

	public void KillPlayer()
	{
		photonView.RPC( "KillPlayer_RPC", PhotonTargets.All );
		// PhotonNetwork.Destroy(this.gameObject);
	}

	[PunRPC]
	public void KillPlayer_RPC()
	{
		// Kill Myself if I'm in charge
		if ( photonView.isMine )
		{
			var death_screen = Instantiate(deathScreenPrefab);
			death_screen.transform.position = this.transform.position;
			death_screen.transform.rotation = this.transform.rotation;
			PhotonNetwork.Destroy(this.gameObject);
		}
	}
}
