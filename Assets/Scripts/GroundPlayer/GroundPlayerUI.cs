﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// Script to Conotrol the Icons on the Ground Player's UI
public class GroundPlayerUI : MonoBehaviour 
{
	[SerializeField] private Button holdMusicButton = null;
	[SerializeField] private Text holdToChargeText = null;

	[SerializeField] private string holdToChargeDescription = "Hold Space to Charge Generator";
	[SerializeField] private string chargingDescription = "Charging";

	[HideInInspector] public GroundPlayerMusicBoxPlayer musicPlayer = null;
	[HideInInspector] public GroundPlayerActivator activator = null;

	void OnEnable()
	{
		StartCoroutine(updateHoldButtonStatus());
		StartCoroutine(updateActivatorText());
	}

	void OnDisable()
	{
		StopAllCoroutines();
	}

	IEnumerator updateHoldButtonStatus()
	{
		while ( true )
		{
			if ( musicPlayer != null )
				holdMusicButton.interactable = ! musicPlayer.isPlaying;

			yield return null;
		}
	}

	IEnumerator updateActivatorText()
	{
		while ( true )
		{
			if ( activator && activator.currentActivator != null )
			{
				holdToChargeText.enabled = true;

				if ( ! activator.charging )
				{
					holdToChargeText.text = holdToChargeDescription;
				}
				else
				{
					holdToChargeText.text = chargingDescription;
				}
			}
			else
			{
				holdToChargeText.enabled = false;
			}

			yield return null;
		}
	}
}
