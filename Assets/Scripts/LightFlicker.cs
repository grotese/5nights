﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class LightFlicker : Photon.MonoBehaviour, ILightSource
{
	private const float minFlickerTime = 0.0f;

	[SerializeField] [Range( minFlickerTime, 20.0f )] private float maxFlickerTime = 5.0f;

	private float timebetweenFlicker = 0.0f;
	private IEnumerator flickerCoroutine = null;
	
	void Start()
	{
		timebetweenFlicker = Random.Range( minFlickerTime, maxFlickerTime );   // Get how long to wait before flicking off then on again
	}

	void OnEnable()
	{
		// isFlickering = true;
		if ( photonView.isMine )
			this.lightState = this.GetComponent<Light>().enabled ? LightState.ON : LightState.OFF;
	}

	void OnDisable()
	{
		// isFlickering = false;
	}

	// Cororutine that just flickers the light on and off like crazy ( never stops )
	IEnumerator flicker()
	{
		float elapsed_time = 0.0f;
		float inter_flicker_time = 0.05f;

		// Debug.Log("Flickering Started");

		while ( true )
		{
			elapsed_time += Time.deltaTime;
			if ( this.GetComponent<Light>().enabled && elapsed_time >= inter_flicker_time )
			{
				elapsed_time = elapsed_time % inter_flicker_time;
				this.GetComponent<Light>().enabled = false;
			}
			else if ( ! this.GetComponent<Light>().enabled && elapsed_time >= inter_flicker_time )
			{
				elapsed_time = elapsed_time % inter_flicker_time;
				this.GetComponent<Light>().enabled = true;
			}

			yield return null;
		}
	}

#region Network

	[PunRPC]
	void changeLightState_RPC( byte byte_light_state )
	{
		LightState light_state = (LightState) byte_light_state;

		if ( _lightState == light_state ) return;    // Don't do anything, if nothing changed
		_lightState = light_state;

		// Turn off flickering coroutine
		if ( flickerCoroutine != null )
		{
			StopCoroutine( flickerCoroutine );
			flickerCoroutine = null;
		}

		switch ( _lightState )
		{
			case LightState.ON:
				this.GetComponent<Light>().enabled = true;
				break;
			case LightState.OFF:
				this.GetComponent<Light>().enabled = false;
				break;
			case LightState.FLICKERING:
				StartCoroutine( flickerCoroutine = flicker() );
				break;
		}
	}

#endregion

#region ILightSource

	protected LightState _lightState = LightState.ON;
	public LightState lightState
	{
		get
		{
			return _lightState;
		}
		set
		{
			if ( PhotonNetwork.inRoom )
				photonView.RPC( "changeLightState_RPC", PhotonTargets.All, (byte) value );    // Tell my local version, and all remote versions that it's flicking time
		}
	}

	protected bool _isRunningIndependentLightFlickering = false;
	public bool isRunningIndependentLightFlickering 
	{ 
		get { return _isRunningIndependentLightFlickering; }
		set { _isRunningIndependentLightFlickering = value; }
	}

#endregion

}
