﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Path Node is used by the MONSTER!!!!!! to navigate around the map
public class PathNodeInfo : MonoBehaviour 
{
	[Tooltip("Nodes this node can navigate to")]
	public List<PathNodeInfo> connectedPathNodes = new List<PathNodeInfo>();

	[Tooltip("All of the lights that illuminate this node")]
	public List<LightFlicker> lights = new List<LightFlicker>();

	[Tooltip("All Cameras that can view this node")]
	public List<SecurityCamera> cameras = new List<SecurityCamera>();

	void Start()
	{
		connectedPathNodes.RemoveAll( node => node == null );    // Remove NUll Fields, nobody needs em
		connectedPathNodes.RemoveAll( node => node == this );    // Remove Circular References

		// Verify all of my children know me, and I know them
		foreach ( PathNodeInfo path_node in connectedPathNodes )
		{
			// Add Myself to all my children, to make sure they know what's up
			if ( ! path_node.connectedPathNodes.Contains( this ) )
			{
				path_node.connectedPathNodes.Add( this );
			}
		}
	}
}
