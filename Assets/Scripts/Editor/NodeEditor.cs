﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

// I want this editor to be able to right click and make a child node of this node, in the game world
// Hopefully this will be able to allow a shortcut key
public class NodeEditor
{
	[MenuItem("Path Nodes/Create Child Node", true)]
	private static bool ValidateCreateChildNode()
	{
		GameObject go = Selection.activeObject as GameObject;
		
		if (go == null)
			return false;

		// Make Sure we have a PathNodeInfo and we are a prefab, Note this doesn't GAURENTEE we are the PathNode Prefab but it's good enough
		return go.GetComponent<PathNodeInfo>() != null && PrefabUtility.GetPrefabParent( go ) != null;
	}


    [MenuItem("Path Nodes/Create Child Node")]
	private static void CreateChildNode()
	{
		GameObject selected_path_node = Selection.activeGameObject;
		Object prefab_root = PrefabUtility.GetPrefabParent (Selection.activeGameObject);    // Grab Prefab information from the selected Object
		GameObject new_node_go = null;

		if ( prefab_root != null )
		{
			new_node_go = PrefabUtility.InstantiatePrefab( 
				prefab_root
			) as GameObject;
		}
		else
		{
			Debug.LogWarning("Trying to Instantiate PathNode that isn't even linked to a Prefab, what an asshole! ");
			return;
		}

		Transform transform = selected_path_node.GetComponent<Transform>();

		new_node_go.GetComponent<Transform>().position = transform.position;
		new_node_go.GetComponent<Transform>().rotation = transform.rotation;

		// Clear the connection nodes
		PathNodeInfo new_node = new_node_go.GetComponent<PathNodeInfo>();
		new_node.connectedPathNodes.Clear();
		new_node.connectedPathNodes.Add( selected_path_node.GetComponent<PathNodeInfo>() ); // Add the Parent as the only Node in the tree

		Selection.activeGameObject = new_node_go;    // Set As Current Selected Object
	}
}

#endif