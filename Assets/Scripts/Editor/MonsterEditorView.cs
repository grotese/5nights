﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MonsterStateMachine))]
public class MonsterEditorView : Editor 
{
	// Use this for initialization
	void OnSceneGUI()
	{
		MonsterStateMachine actual_target = target as MonsterStateMachine;

		Handles.color = new Color( 1f, 0f, 0f, 0.1f );
		Handles.DrawSolidDisc( 
			actual_target.transform.position, 
			Vector3.up, 
			actual_target.visionRange
		);
		Handles.color = Color.red;
		
		actual_target.visionRange = Handles.ScaleValueHandle(
			actual_target.visionRange,
			actual_target.transform.position + new Vector3(actual_target.visionRange,0,0),
			Quaternion.identity,
			2,
			Handles.CylinderCap,
			2
		);
	}
}

#endif
