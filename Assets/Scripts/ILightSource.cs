﻿public enum LightState : byte
{
	ON = 0,
	OFF,
	FLICKERING
}

public interface ILightSource
{
	LightState lightState { get; set; }
	bool isRunningIndependentLightFlickering { get; set; }
}
