﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleableColor : MonoBehaviour 
{
	[SerializeField] private Image targetImage = null;
	[SerializeField] private Color toggledColor = Color.white;
	private Color initialColor = Color.white;

	// Use this for initialization
	void Start () 
	{
		initialColor = targetImage.color;    // save initial image
	}

	//  Assign this for the On Value Changed
	public void OnToggleValueChange( bool value )
	{
		if ( value )    // If now toggled, then change color to toggled color
		{
			targetImage.color = toggledColor;
		}
		else
		{
			// else set it back to default
			targetImage.color = initialColor;	
		}
	}
	
}
