﻿using System;
using UnityEngine;

public static class GlobalGameData
{
	private static WeakReference _groundPlayer;
	public static GameObject groundPlayer
	{
		get
		{
			if ( _groundPlayer != null )
			{
				return _groundPlayer.Target as GameObject;
			}
			return null;
		}
		set
		{
			if ( _groundPlayer == null )
			{
				_groundPlayer = new WeakReference(value);
			}
			else
			{
				_groundPlayer.Target = value;
			}
		}
	}

	private static WeakReference _cameraPlayer;
	public static GameObject cameraPlayer
	{
		get
		{
			if ( _cameraPlayer != null )
			{
				return _cameraPlayer.Target as GameObject;
			}
			return null;
		}
		set
		{
			if ( _cameraPlayer == null )
			{
				_cameraPlayer = new WeakReference(value);
			}
			else
			{
				_cameraPlayer.Target = value;
			}
		}
	}

	private static WeakReference _cameraSystem;
	public static GameObject cameraSystem
	{
		get
		{
			if ( _cameraSystem != null )
			{
				return _cameraSystem.Target as GameObject;
			}
			return null;
		}
		set
		{
			if ( _cameraSystem == null )
			{
				_cameraSystem = new WeakReference(value);
			}
			else
			{
				_cameraSystem.Target = value;
			}
		}
	}

	private static WeakReference _powerResource;
	public static PowerResource powerResource
	{
		get
		{
			if ( _powerResource != null )
			{
				return _powerResource.Target as PowerResource;
			}
			return null;
		}
		set
		{
			if ( _powerResource == null )
			{
				_powerResource = new WeakReference(value);
			}
			else
			{
				_powerResource.Target = value;
			}
		}
	}
}
