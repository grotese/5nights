using UnityEngine;
using System.Collections;

public class PowerResource : Photon.MonoBehaviour 
{

#region Monobehavior Methods

	private void OnEnable()
	{
		GlobalGameData.powerResource = this;
	}

	private void OnDisable()
	{
		GlobalGameData.powerResource = null;
	}

	private void Update()
	{
		if ( photonView.isMine )
		{
			modifyPowerLevel( -powerLevelLossRate * Time.deltaTime );
		}
	}

#endregion

	[SerializeField] protected float powerLevelLossRate = 1.0f;
	[SerializeField] public float maxPower = 100.0f;

	[SerializeField] protected float _powerLevel = 100;
	public float powerLevel 
	{ 
		get { return _powerLevel; }
	}

	public void modifyPowerLevel( float change_in_power )
	{
		photonView.RPC( "modifyPowerLevel_RPC", PhotonTargets.All, change_in_power );
	}

#region Network Junk

	[PunRPC]
	protected void modifyPowerLevel_RPC( float change_in_power )
	{
		// Only Modify if it's mine
		if ( photonView.isMine )
		{
			_powerLevel = Mathf.Clamp( 
				_powerLevel + change_in_power,
				0,
				maxPower
			);
		}
	}

	private void OnPhotonSerializeView( PhotonStream stream, PhotonMessageInfo info ) 
	{
		// Send Upates through the network
		if ( stream.isWriting )
		{
			stream.SendNext( _powerLevel );
		}
		else // reading
		{
			_powerLevel = (float) stream.ReceiveNext();
		}
	}

#endregion

}
