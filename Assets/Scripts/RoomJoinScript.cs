﻿using UnityEngine;
using System.Collections;
//using UnityEngine.Events;

// This Script is intended to be created after the player successfulyl joins a room, but before he spawns as a player
// This script will handle how the player spawns
[RequireComponent(typeof(Canvas))]
public class RoomJoinScript : MonoBehaviour 
{
	[SerializeField] private UnityEngine.UI.Button SpawnsAsCameraPlayerButton = null;
	[SerializeField] private UnityEngine.UI.Button SpawnsAsGroundPlayerButton = null;
	[HideInInspector] public NetworkManager localNetworkManager = null;

	// Use this for initialization
	void Start () 
	{
		// Verify Inspector Data

		// Find the current State of the Room
		if ( PhotonNetwork.connectionStateDetailed != PeerState.Joined )
		{
			// Nothing should be visible, DISABLE EVERYTHING!
			SpawnsAsCameraPlayerButton.interactable = false;
			SpawnsAsGroundPlayerButton.interactable = false;
		}
		else
		{
			// Pull all other player information
			foreach ( PhotonPlayer other_player in PhotonNetwork.otherPlayers )
			{
				ExitGames.Client.Photon.Hashtable custom_properties = other_player.customProperties;

				if ( custom_properties.ContainsKey( "PlayerType" ) )
				{
					switch ( (PlayerType) custom_properties[ "PlayerType" ] )
					{
						case PlayerType.CameraPlayer:
							SpawnsAsCameraPlayerButton.interactable = false;
							break;
						case PlayerType.GroundPlayer:
							SpawnsAsGroundPlayerButton.interactable = false;
							break;
					}
				}
			}
		}
	}
	
#region UI Callbacks

	public void OnClickSpawnsAsCameraPlayerButton()
	{
		// Tell properties that I am now the camera player
		ExitGames.Client.Photon.Hashtable custom_properties = new ExitGames.Client.Photon.Hashtable()
		{
			{ "PlayerType", PlayerType.CameraPlayer }
		};

		PhotonNetwork.SetPlayerCustomProperties( custom_properties );

		// Spawn player as Camera Man
		localNetworkManager.SpawnLocalPlayer( PlayerType.CameraPlayer );

		// Delete this menu, I don't like it anymore
		GameObject.Destroy( this.gameObject);
	}

	public void OnClickSpawnsAsGroundPlayerButton()
	{
		// Tell properties that I am now the ground player
		ExitGames.Client.Photon.Hashtable custom_properties = new ExitGames.Client.Photon.Hashtable()
		{
			{ "PlayerType", PlayerType.GroundPlayer }
		};

		PhotonNetwork.SetPlayerCustomProperties( custom_properties );

		// Spawn player as Ground Man	
		localNetworkManager.SpawnLocalPlayer( PlayerType.GroundPlayer );

		// Delete this menu, I don't like it anymore
		GameObject.Destroy( this.gameObject);
	}

#endregion

#region Photon Callbacks

	// Photon Callbacks
	void OnJoinedRoom()
	{
		Debug.Log("I Joined the Room!");

		// Default enabled
		SpawnsAsCameraPlayerButton.interactable = true;
		SpawnsAsGroundPlayerButton.interactable = true;

		// What Players have already joined, what player are they registered as?
		// Disable button that refers to player that has already spawned
		// Pull all other player information
		foreach ( PhotonPlayer other_player in PhotonNetwork.otherPlayers )
		{
			ExitGames.Client.Photon.Hashtable custom_properties = other_player.customProperties;

			if ( custom_properties.ContainsKey( "PlayerType" ) )
			{
				switch ( (PlayerType) custom_properties[ "PlayerType" ] )
				{
					case PlayerType.CameraPlayer:
						Debug.Log("Another Player is already the camera player");
						SpawnsAsCameraPlayerButton.interactable = false;
						break;
					case PlayerType.GroundPlayer:
						Debug.Log("Another Player is already the ground player");
						SpawnsAsGroundPlayerButton.interactable = false;
						break;
				}
			}
		}
	}

	// If a player spawned as a player, then I need to disable the option to spawn as that player
	void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
	{
		//PhotonPlayer other_player                           = playerAndUpdatedProps[0] as PhotonPlayer;
        ExitGames.Client.Photon.Hashtable custom_properties = playerAndUpdatedProps[1] as ExitGames.Client.Photon.Hashtable;

        //if ( other_player.isLocal ) return;    // It's mE!

        if ( custom_properties.ContainsKey( "PlayerType" ) )
		{
			switch ( (PlayerType) custom_properties[ "PlayerType" ] )
			{
				case PlayerType.CameraPlayer:
					SpawnsAsCameraPlayerButton.interactable = false;
					break;
				case PlayerType.GroundPlayer:
					SpawnsAsGroundPlayerButton.interactable = false;
					break;
			}
		}
	}

	// If a player LEFT, then I need I need to re-enable that button, if they had already spawned
	void OnPhotonPlayerDisconnected(PhotonPlayer other_player)
	{
		ExitGames.Client.Photon.Hashtable custom_properties = other_player.customProperties;

		if ( custom_properties.ContainsKey( "PlayerType" ) )
		{
			switch ( (PlayerType) custom_properties[ "PlayerType" ] )
			{
				case PlayerType.CameraPlayer:
					SpawnsAsCameraPlayerButton.interactable = true;
					break;
				case PlayerType.GroundPlayer:
					SpawnsAsGroundPlayerButton.interactable = true;
					break;
			}
		}
	}

#endregion

}
