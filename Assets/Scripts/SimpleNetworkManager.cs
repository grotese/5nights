﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This Simple Network Manager just handles disable/enabling local components
// and syncing enabled states with specific components
// USE ONLY FOR SCENE OBJECTS, NOT ONES MADE VIA PhotonNetwork.Instantiate!
public class SimpleNetworkManager : Photon.MonoBehaviour 
{
	[TooltipAttribute("Components to disable if remote, or keep enabled if local")]
	[SerializeField] private List< Behaviour > localLogicComponents = new List< Behaviour >();

	[TooltipAttribute( "Components to monitor for 'enable' field state changes" )]
	[SerializeField] private Behaviour[] syncEnabledStateComponents = new Behaviour[0];

	// Use this for initialization
	void OnJoinedRoom()
	{
		if ( photonView.isMine )
		{
			foreach ( Behaviour logic in localLogicComponents )
			{
				logic.enabled = true;
			}
		}
		else
		{
			foreach ( Behaviour logic in localLogicComponents )
			{
				logic.enabled = false;
			}
		}
	}
	
	void OnPhotonSerializeView( PhotonStream stream, PhotonMessageInfo info) 
	{
		// Send Upates through the network
		if ( stream.isWriting )
		{
			foreach ( Behaviour components in syncEnabledStateComponents )
			{
				stream.SendNext( components.enabled );
			}
		}
		else // reading
		{
			foreach ( Behaviour components in syncEnabledStateComponents )
			{
				components.enabled = (bool) stream.ReceiveNext();
			}
		}
	}
}
