﻿using UnityEngine;
using UnityEngine.UI;

public class MinimapHandler : MonoBehaviour 
{
	[SerializeField] private Camera miniMapCamera = null;
	[SerializeField] private Camera staticScreenCamera = null;
	[SerializeField] private RectTransform miniMapCanvasRect = null;
	[SerializeField] private RectTransform minimapCameraFeed = null;

	[SerializeField] private Vector2 minimapTargetSize = new Vector2( 512, 512 );

	[SerializeField] private GameObject securityCameraMinimapButton = null;

	[SerializeField] private Button doorButton = null;
	[SerializeField] private AudioSource screenAudio = null;
	[SerializeField] private AudioSource cameraChangeAudio = null;

	public delegate void OnCameraChange(ISecurityCamera security_camera);
	public event OnCameraChange CameraChange = delegate {};
	/// Called When teh current camera's state chanegs
	public event StateChangeHandler CameraStateChange = delegate {};

	private ISecurityCamera currentSelectedCamera = null;
	private Toggle currentSelectedCameraToggle = null;

	private void Start()
	{
		GlobalGameData.cameraSystem = this.gameObject;
	}

	private void OnEnable()
	{
		// Find all those Cameras, ya punk
		GameObject[] securityCameras = GameObject.FindGameObjectsWithTag( "SecurityCamera" );
		Rect canvas_rect = miniMapCanvasRect.rect;

		// Foreach camera, make a new button on the minimap
		foreach ( GameObject security_camera in securityCameras )
		{
			// Make Sure this Object has a SecurityCamera Component
			SecurityCamera security_camera_component = security_camera.GetComponent<SecurityCamera>();
			if ( security_camera_component == null ) continue;

			// Find Camera Position
			Transform security_cam_trans = security_camera.GetComponent<Transform>();
			Vector3 screen_coordinates = miniMapCamera.WorldToScreenPoint( security_cam_trans.position );    // Upcast from Camera ( will be coordinates based off size of RenderTexture )

			// From screen coordinates, convert to Canvas Coordinates
			Vector2 canvas_position = new Vector2(
				screen_coordinates.x * canvas_rect.width  / minimapTargetSize.x,
				screen_coordinates.y * canvas_rect.height / minimapTargetSize.y
			);

			// With these Coordinates, mkae a new security Camera Button, and set the center to these positions
			GameObject security_camera_button = Instantiate( securityCameraMinimapButton ) as GameObject;
			security_camera_button.GetComponent<RectTransform>().SetParent(minimapCameraFeed, false);    // Set Parent as the Camera Feed UI Element
			security_camera_button.GetComponent<RectTransform>().anchoredPosition = canvas_position;    // Set Position

			security_camera_component.securityCamera.enabled = false;    // Disable Security Camera

			// Grab toggle from security camera button
			Toggle toggle = security_camera_button.GetComponent<Toggle>();
			// Add lambda that only calls Method if the value is true, ignore falses,
			toggle.onValueChanged.AddListener( 
				(bool value) => 
				{
					if ( value )
					{
						OnSecurityCameraClick( toggle, security_camera_component );
					}
				}
			);
		}

		// Disable to OFF, but don't play static, wait for Camera Player to actually join
		staticScreenCamera.enabled = true;
		doorButton.interactable = false;
	}

	private void OnDestroy()
	{
		GlobalGameData.cameraSystem = null;
	}

	private void OnDoorButtonPressed( ISealableDoor door )
	{
		door.toggleDoor();
	}

	// Method Callback fro when the currently selected security camera state changes
	private void onCurrentSecurityCameraStateChange( ISecurityCamera security_camera, SecurityCameraState new_state )
	{
		switch ( new_state )
		{
			case SecurityCameraState.OFF:
				// Set Camera, instead to the static screen camera, ya dumb fuck
				staticScreenCamera.enabled = true;
				security_camera.securityCamera.enabled = false;
				break;
			default:
				// Set Camera, instead to the static screen camera, ya dumb fuck
				staticScreenCamera.enabled = false;
				security_camera.securityCamera.enabled = true;
				break;
		}

		CameraStateChange( security_camera, new_state );
	}

	private void OnSecurityCameraClick( Toggle toggle, ISecurityCamera security_camera )
	{
		staticScreenCamera.enabled = false;    // Turn off Static Screen Camera

		// Switch view to Security Camera
		if ( currentSelectedCamera != null )
		{
			// Disable old security Camera
			currentSelectedCamera.securityCamera.enabled = false;
			doorButton.onClick.RemoveAllListeners();    // Remove Listeners to previous security camera door
			doorButton.interactable = false;            // Mark it as uninteractable

			// Unregister to the camera state change event
			currentSelectedCamera.onStateChange -= onCurrentSecurityCameraStateChange;
		}

		// If the Toggle has been selected before, then set it to off, and also set it to interactable
		if ( currentSelectedCameraToggle != null )
		{
			currentSelectedCameraToggle.isOn = false;
			currentSelectedCameraToggle.interactable = true;
		}

		// Setup New Camera
		currentSelectedCamera = security_camera;    // Assign Currently selected Security Camera
		currentSelectedCameraToggle = toggle;

		// Toggle Should now be ON, but we need to make it non-interactable so they can't press it again
		currentSelectedCameraToggle.interactable = false;

		// TODO: Check if Camera is ON, Fuzzy, etc
		currentSelectedCamera.onStateChange += onCurrentSecurityCameraStateChange;    // Register to events

		// Make a faux call to the event handler so we don't have to duplicate code
		onCurrentSecurityCameraStateChange( currentSelectedCamera, currentSelectedCamera.securityCameraState );

		// Change Door Button Control
		if ( currentSelectedCamera.door != null )
		{ 
			doorButton.onClick.AddListener( () => OnDoorButtonPressed( currentSelectedCamera.door ) );
			doorButton.interactable = true;
		}
		else
		{
			doorButton.interactable = false;
		}

		CameraChange( security_camera );
	}

	public void setUICamera( Camera camera )
	{
		foreach ( Canvas canvas in this.GetComponentsInChildren<Canvas>() )
		{
			canvas.worldCamera = camera;
		}

		// Set Starting Camera as Static Screen
		staticScreenCamera.enabled = true;
		doorButton.interactable = false;
		screenAudio.Play();
	}
}
