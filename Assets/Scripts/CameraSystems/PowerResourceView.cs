﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PowerResourceView : MonoBehaviour 
{
	[SerializeField] private Slider powerLevelSlider = null;

	void OnEnable()
	{
		StartCoroutine(lerpPowerSlider());
	}

	void OnDisable()
	{
		StopAllCoroutines();		
	}

	IEnumerator lerpPowerSlider()
	{
		while ( true )
		{
			if ( GlobalGameData.powerResource == null )
			{
				yield return null;
				continue;				
			}

			powerLevelSlider.value = Mathf.Lerp(
				powerLevelSlider.value, 
				GlobalGameData.powerResource.powerLevel, 
				Time.deltaTime
			);

			yield return null;
		}
	}
}
