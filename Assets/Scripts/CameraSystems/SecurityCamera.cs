﻿using UnityEngine;

public class SecurityCamera : MonoBehaviour, ISecurityCamera
{
#region ISecurityCamera
	
	public event StateChangeHandler onStateChange = delegate{};

	protected SecurityCameraState _securityCameraState = SecurityCameraState.ON;
	public SecurityCameraState securityCameraState
	{ 
		get { return _securityCameraState; }
		set 
		{ 
			_securityCameraState = value; 
			onStateChange( this, _securityCameraState );
		}
	}

	[SerializeField] private Camera _camera = null;
	public Camera securityCamera { get { return _camera; } }

	[SerializeField] private SealableDoor _door = null;
	public ISealableDoor door { get { return _door; } }

#endregion
}
