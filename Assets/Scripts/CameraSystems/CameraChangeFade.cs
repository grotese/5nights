﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraChangeFade : MonoBehaviour 
{
	[SerializeField] private RawImage fadeInImage = null;
	[SerializeField] private MinimapHandler minimap = null;

	[SerializeField] private float fadeTime = 1.0f;

	void OnEnable()
	{
		minimap.CameraChange += OnCameraChange;
	}

	void OnDisable()
	{
		minimap.CameraChange -= OnCameraChange;		
	}

	void OnCameraChange( ISecurityCamera security_camera )
	{
		StopAllCoroutines();
		StartCoroutine(securityCameraFadeOut());
	}

	IEnumerator securityCameraFadeOut()
	{
		fadeInImage.color = fadeInImage.color + new Color( 0, 0, 0, 1.0f );    // Start at 100% alpha

		float rate = 1.0f / fadeTime;
		Color temp_color = fadeInImage.color;

		// Go from 1.0f alpha to 0.0f in fadeTime seconds
		for ( float elapsed_time = 0.0f; elapsed_time < fadeTime ; elapsed_time += Time.deltaTime )
		{
			temp_color = fadeInImage.color;

			temp_color.a = 1 - elapsed_time / fadeTime;

			fadeInImage.color = temp_color;

			yield return null;
		}
	}
}
