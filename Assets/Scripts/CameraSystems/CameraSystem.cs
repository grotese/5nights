﻿using UnityEngine;

// This Script will Change the Security Camera View on the Camera Systems Canvas
// In the Future, this may also be part of the Dynamic Building of the Security Camera View, based of all security Cameras in the scene
public class CameraSystem : MonoBehaviour 
{
	// List of all the Cameras we can switch to
	[SerializeField] private SecurityCamera[] cameraList = new SecurityCamera[0];
	private int currentCameraIndex = 0;

	// Callback for when the player presses the switch camera button
	public void OnCameraSwitchButton()
	{
		if ( cameraList.Length == 0 ) return;    // The list is empty

		// Disable Current Camera and Enable Future one
		cameraList[ currentCameraIndex ].securityCamera.enabled = false;
		currentCameraIndex = ( currentCameraIndex + 1 ) % cameraList.Length;  // Cycle onto the next camera
		cameraList[ currentCameraIndex ].securityCamera.enabled = true;    // Enabled New Camaera
	}

	// Use this for initialization
	void Start () 
	{
		if ( cameraList.Length == 0 ) return;    // The list is empty

		foreach ( ISecurityCamera cam in cameraList )
		{
			cam.securityCamera.enabled = false;
		}

		cameraList[ currentCameraIndex ].securityCamera.enabled = true;    // Enabled New Camaera
	}	
}
