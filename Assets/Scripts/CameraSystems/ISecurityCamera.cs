using UnityEngine;

[System.Flags]
public enum SecurityCameraState : byte
{
	OFF            = 0x0,              // Off is used by external forces ( monsters ) to temporarily turn off cameras
	ON             = 0x1,              // ON means it's visible, perfect
	TRANSITIONING  = 0x2,              // the camera was either just off or switched to and now it coming into focus
	DISABLED       = 0x4,              // the camera is broken and external forces cannot turn it on or off
	STATIC_PLAYING = OFF | DISABLED | TRANSITIONING   
}

public delegate void StateChangeHandler( ISecurityCamera camera, SecurityCameraState new_state );

/// <summary>
/// Security Camera Interface.
/// Defines how the Security Camera System and Monsters interface with security Cameras
/// </summary>
public interface ISecurityCamera
{
	event StateChangeHandler onStateChange;
	SecurityCameraState securityCameraState { get; set; }
	Camera securityCamera { get; }
	ISealableDoor door { get; }
}

