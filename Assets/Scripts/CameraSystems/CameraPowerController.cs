﻿using UnityEngine;
using System.Collections;

public class CameraPowerController : MonoBehaviour 
{
	[SerializeField] private GameObject[] monitors = new GameObject[2];
	[SerializeField] private AudioSource powerAudio = null;
	[SerializeField] private AudioClip powerOffSound = null;
	[SerializeField] private AudioClip powerOnSound = null;

	void OnEnable()
	{
		StartCoroutine( listenForPowerDepletion() );
	}

	void OnDisable()
	{
		StopAllCoroutines();
	}

	IEnumerator listenForPowerDepletion()
	{
		while ( true )
		{
			if ( GlobalGameData.powerResource != null )
			{
				if ( GlobalGameData.powerResource.powerLevel <= 0 )				
				{
					// Turn off monitors
					foreach ( GameObject monitor in monitors )
					{
						monitor.SetActive(false);
					}

					powerAudio.clip = powerOffSound;
					powerAudio.Play();

					yield return StartCoroutine(listenForPowerRestore());
				}
			}

			yield return null;
		}
	}

	IEnumerator listenForPowerRestore()
	{
		while ( true )
		{
			if ( GlobalGameData.powerResource != null )
			{
				if ( GlobalGameData.powerResource.powerLevel > 0 )				
				{
					// Turn off monitors
					foreach ( GameObject monitor in monitors )
					{
						monitor.SetActive(true);
					}

					powerAudio.clip = powerOnSound;
					powerAudio.Play();

					yield return StartCoroutine(listenForPowerDepletion());
				}
			}
			else
			{
				Debug.LogWarning("Power is off, but there is no power source!");
			}

			yield return null;
		}
	}
}
