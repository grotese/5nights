﻿using UnityEngine;
using System.Collections;

public class StaticScreenEffect : MonoBehaviour 
{
	[SerializeField] private MeshRenderer meshRenderer = null;
	
	[SerializeField] private Texture[] staticImages;
	[SerializeField] private float timeBetweenFrames = 0.075f;
	[SerializeField] private bool loop = true;

	// Update is called once per frame
	void Start() 
	{
		StartCoroutine(cycleFrames());
	}

	IEnumerator cycleFrames()
	{
		do
		{
			for ( int index = 0; index < staticImages.Length ; ++index )
			{
				meshRenderer.material.mainTexture = staticImages[ index ];
				yield return new WaitForSeconds(timeBetweenFrames);
			}
		} while( loop );
	}
}
