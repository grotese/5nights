﻿using UnityEngine;
using System.Collections;

// This class will listen for events and then play audio accordingly
public class CameraSystemAudio : MonoBehaviour 
{
	[SerializeField] private MinimapHandler minimap = null;
	[SerializeField] private AudioSource screenAudio = null;
	[SerializeField] private AudioSource cameraChangeAudio = null;

	void OnEnable()
	{
		minimap.CameraChange += OnCameraChange;
		minimap.CameraStateChange += OnCameraStateChange;
	}

	void OnDisable()
	{
		minimap.CameraChange -= OnCameraChange;
		minimap.CameraStateChange -= OnCameraStateChange;
	}

#region Event Handlers

	void OnCameraChange( ISecurityCamera security_camera )
	{
		cameraChangeAudio.Play();    // Play Camera Switch ( TODO: Move Audio Handling into separate script )

		// Play Static if it's non existent, or disabled
		if ( security_camera == null || security_camera.securityCameraState == SecurityCameraState.OFF )
		{
			screenAudio.Play();
		}
		else
		{
			screenAudio.Stop();
		}
	}

	void OnCameraStateChange( ISecurityCamera security_camera, SecurityCameraState new_state )
	{
		switch ( new_state )
		{
			case SecurityCameraState.OFF:
				screenAudio.Play();
				break;
			default:
				screenAudio.Stop();
				break;
		}
	}

#endregion

}
