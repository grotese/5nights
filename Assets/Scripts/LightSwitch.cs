﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
[RequireComponent(typeof(PhotonView))]
public class LightSwitch : MonoBehaviour 
{
	private float elapsedTime = 0.0f;
	private float switchTime = 1.0f;

	void Update()
	{
		if ( this.GetComponent<PhotonView>().isMine )
		{
			// Do logic only if this is my light!Q
			elapsedTime += Time.deltaTime;
			if ( elapsedTime >= switchTime )
			{
				setLightEnabled( ! this.GetComponent<Light>().enabled );
				elapsedTime = elapsedTime % switchTime;
			}
		}
	}

	public void setLightEnabled( bool enabled )
	{
		this.GetComponent<PhotonView>().RPC( "setLightEnabled_RPC", PhotonTargets.All, enabled );
	}

	[PunRPC]
	void setLightEnabled_RPC( bool enabled )
	{
		this.GetComponent<Light>().enabled = enabled;
	}
}
