﻿Shader "Custom/MinimapWallShader" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader 
	{
		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			// Use shader model 3.0 target, to get nicer looking lighting
			uniform float4 _Color;

			struct vertexInput
			{
				float4 vertex : POSITION;
			};

			struct vertexOutput
			{
				float4 pos : SV_POSITION;
			};

			vertexOutput vert( vertexInput input )
			{
				vertexOutput output;
				output.pos = mul( UNITY_MATRIX_MVP, input.vertex );
				return output;
			}

			float4 frag( vertexOutput input ) : COLOR
			{
				return _Color;
			}

			ENDCG
		}
	} 
	// FallBack "Diffuse"
}
